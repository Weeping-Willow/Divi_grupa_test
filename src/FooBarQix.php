<?php

declare(strict_types=1);
class FooBarQix
{
    public function fooBarQixOutput(int $positiveInteger)
    {
        //if number is a multiple of the keys output the corresponding value or contains the said number
        $ruleSetMultiplyOrContainForFunction = array(
            "3"  => "Foo",
            "5" => "Bar",
            "7" => "Qix",
        );

        $output = "";

        if ($this->isNegativeIntegerOrZero($positiveInteger)) {
            $output = strval($positiveInteger);
            throw new InvalidArgumentException("Input is less than 1 " . $positiveInteger);
        } else {
            $output =  $output . $this->addNeededStringIfIntegerCompliesWithMultiplyRuleSet($positiveInteger, $ruleSetMultiplyOrContainForFunction, "");

            $output = $output . $this->addNeededStringIfIntegerCompliesWithContainsRuleSet($positiveInteger, $ruleSetMultiplyOrContainForFunction, "");
        }

        if ($this->doesntMatchAnyOfTheRules($output)) {
            $output = strval($positiveInteger);
        }

        return $output;
    }

    public function infQixFooService(int $positiveInteger)
    {
        //if number is a multiple of the keys output the corresponding value or contains the said number
        $ruleSetMultiplyOrContainForFunction = array(
            "8"  => "Inf",
            "7" => "Qix",
            "3" => "Foo",
        );

        //if sum of all individual numbers is equal to key output the corresponding value
        $ruleSetSumOfAllIndividualNumbersForFunction = array(
            "8" => "Inf",
        );

        //Dividers for each of the previous rules sets
        $ruleSetForDividingElementsFromTheSameRuleSet = array(
            "Multiply" => ";",
            "Sum" => "",
            "Contains" => ""
        );

        $output = "";

        if ($this->isNegativeIntegerOrZero($positiveInteger) || !is_int($positiveInteger)) {
            $output = strval($positiveInteger);
            throw new InvalidArgumentException("Input is less than 1 " . $positiveInteger);
        } else {
            $output =  $output . $this->addNeededStringIfIntegerCompliesWithMultiplyRuleSet($positiveInteger, $ruleSetMultiplyOrContainForFunction, $ruleSetForDividingElementsFromTheSameRuleSet["Multiply"]);

            $output = $output . $this->addNeededStringIfIntegerCompliesWithContainsRuleSet($positiveInteger, $ruleSetMultiplyOrContainForFunction, $ruleSetForDividingElementsFromTheSameRuleSet["Contains"]);

            $output = $output . $this->addNeededStringIfIntegerCompliesWithSumRuleSet($positiveInteger, $ruleSetSumOfAllIndividualNumbersForFunction, $ruleSetForDividingElementsFromTheSameRuleSet["Sum"]);
        }

        if ($this->doesntMatchAnyOfTheRules($output)) {
            $output = strval($positiveInteger);
        }
        return $output;
    }

    public function isAMultipleOfX($positiveInteger, $xNumber)
    {
        return $positiveInteger % $xNumber == 0;
    }

    public function isNegativeIntegerOrZero($integer)
    {
        return $integer < 1;
    }

    private function addNeededStringIfIntegerCompliesWithMultiplyRuleSet(int $positiveInteger, array $ruleSetForFunction, $divider)
    {
        $output = "";

        foreach ($ruleSetForFunction as $theRuleItem => $theRuleValue) {
            if ($this->isAMultipleOfX($positiveInteger, $theRuleItem)) {
                if ($this->doesntMatchAnyOfTheRules($output)) {
                    $output = $output . $theRuleValue;
                } else {
                    $output = $output . $divider . $theRuleValue;
                }
            }
        }
        return $output;
    }

    private function addNeededStringIfIntegerCompliesWithContainsRuleSet(int $positiveInteger, array $ruleSetForFunction, $divider)
    {
        $output = "";
        $integerAsStringInParts = str_split(strval($positiveInteger));

        for ($iterator = 0; $iterator < sizeof($integerAsStringInParts); $iterator++) {
            if (array_key_exists($integerAsStringInParts[$iterator], $ruleSetForFunction)) {
                if ($this->doesntMatchAnyOfTheRules($output)) {
                    $output = $output . $this->AddOnOfTheRuleSetValue($integerAsStringInParts[$iterator], $ruleSetForFunction, "");
                } else {
                    $output = $output . $this->AddOnOfTheRuleSetValue($integerAsStringInParts[$iterator], $ruleSetForFunction, $divider);
                }
            }
        }
        return $output;
    }

    private function AddOnOfTheRuleSetValue($number, array $ruleSetForFunction, $divider)
    {
        foreach ($ruleSetForFunction as $theRuleItem => $theRuleValue) {
            if ($number == $theRuleItem) {
                return $divider . $theRuleValue;
            }
        }
    }

    private function doesntMatchAnyOfTheRules(string $output)
    {
        return strlen($output) < 1;
    }

    private function addNeededStringIfIntegerCompliesWithSumRuleSet(int $positiveInteger, array $ruleSetForFunction, $divider)
    {
        $integerAsStringInParts = str_split(strval($positiveInteger));
        $sumOfTheNumber = 0;

        for ($iterator = 0; $iterator < sizeof($integerAsStringInParts); $iterator++) {
            $sumOfTheNumber += $integerAsStringInParts[$iterator];
        }

        return $this->addNeededStringIfIntegerCompliesWithMultiplyRuleSet($sumOfTheNumber, $ruleSetForFunction, $divider);
    }
}
