<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require __DIR__ . "/../src/FooBarQix.php";

class FooBarFixTest extends TestCase
{

    public function test0ReturnsAsString()
    {
        $objectFooBarQix = new FooBarQix();
        $this->expectException(InvalidArgumentException::class);
        $this->assertEquals(
            '0',
            $objectFooBarQix->fooBarQixOutput(0)
        );
    }
    public function test1ReturnsAsString()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '1',
            $objectFooBarQix->fooBarQixOutput(1)
        );
    }
    public function test2ReturnsAsString()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '2',
            $objectFooBarQix->fooBarQixOutput(2)
        );
    }
    public function test3ReturnsAsFooFoo()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooFoo',
            $objectFooBarQix->fooBarQixOutput(3)
        );
    }
    public function test4ReturnsAsString()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '4',
            $objectFooBarQix->fooBarQixOutput(4)
        );
    }
    public function test5ReturnsAsBarBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'BarBar',
            $objectFooBarQix->fooBarQixOutput(5)
        );
    }
    public function test6ReturnsAsFoo()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Foo',
            $objectFooBarQix->fooBarQixOutput(6)
        );
    }
    public function test7ReturnsAsQixQix()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'QixQix',
            $objectFooBarQix->fooBarQixOutput(7)
        );
    }
    public function test8ReturnsAsString()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '8',
            $objectFooBarQix->fooBarQixOutput(8)
        );
    }
    public function test9ReturnsAsFoo()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Foo',
            $objectFooBarQix->fooBarQixOutput(9)
        );
    }
    public function test10ReturnsAsBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Bar',
            $objectFooBarQix->fooBarQixOutput(10)
        );
    }
    public function test14ReturnsAsQix()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Qix',
            $objectFooBarQix->fooBarQixOutput(14)
        );
    }
    public function test15ReturnsAsFooBarBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooBarBar',
            $objectFooBarQix->fooBarQixOutput(15)
        );
    }
    public function test21ReturnsAsFooQix()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooQix',
            $objectFooBarQix->fooBarQixOutput(21)
        );
    }
    public function test33ReturnsAsFooFooFoo()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooFooFoo',
            $objectFooBarQix->fooBarQixOutput(33)
        );
    }
    public function test34ReturnsAsFoo()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Foo',
            $objectFooBarQix->fooBarQixOutput(34)
        );
    }
    public function test35ReturnsAsBarQixFooBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'BarQixFooBar',
            $objectFooBarQix->fooBarQixOutput(35)
        );
    }

    public function test42ReturnsAsFooQix()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooQix',
            $objectFooBarQix->fooBarQixOutput(42)
        );
    }
    public function test52ReturnsAsBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Bar',
            $objectFooBarQix->fooBarQixOutput(52)
        );
    }
    public function test55ReturnsAsBarBarBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'BarBarBar',
            $objectFooBarQix->fooBarQixOutput(55)
        );
    }
    public function test57ReturnsAsFooBarQix()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooBarQix',
            $objectFooBarQix->fooBarQixOutput(57)
        );
    }
    public function test58ReturnsAsBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Bar',
            $objectFooBarQix->fooBarQixOutput(58)
        );
    }
    public function test70ReturnsAsBarQixQix()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'BarQixQix',
            $objectFooBarQix->fooBarQixOutput(70)
        );
    }
    public function test71ReturnsAsQix()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Qix',
            $objectFooBarQix->fooBarQixOutput(71)
        );
    }
    public function test73ReturnsAsQixFoo()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'QixFoo',
            $objectFooBarQix->fooBarQixOutput(73)
        );
    }
    public function test100ReturnsAsBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Bar',
            $objectFooBarQix->fooBarQixOutput(100)
        );
    }
    public function test120ReturnsAsFooBar()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooBar',
            $objectFooBarQix->fooBarQixOutput(120)
        );
    }

    public function testNegative1ReturnsNegative1()
    {
        $objectFooBarQix = new FooBarQix();
        $this->expectException(InvalidArgumentException::class);
        $this->assertEquals(
            '-1',
            $objectFooBarQix->fooBarQixOutput(-1)
        );
    }
    public function testNegative3ReturnsAsNegative3()
    {
        $objectFooBarQix = new FooBarQix();
        $this->expectException(InvalidArgumentException::class);
        $this->assertEquals(
            '-3',
            $objectFooBarQix->fooBarQixOutput(-3)
        );
    }
    




    public function test0infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->expectException(InvalidArgumentException::class);
        $this->assertEquals(
            '0',
            $objectFooBarQix->infQixFooService(0)
        );
    }
    public function test1infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '1',
            $objectFooBarQix->infQixFooService(1)
        );
    }
    public function test2infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '2',
            $objectFooBarQix->infQixFooService(2)
        );
    }
    public function test3infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooFoo',
            $objectFooBarQix->infQixFooService(3)
        );
    }
    public function test4infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '4',
            $objectFooBarQix->infQixFooService(4)
        );
    }
    public function test5infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '5',
            $objectFooBarQix->infQixFooService(5)
        );
    }
    public function test6infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Foo',
            $objectFooBarQix->infQixFooService(6)
        );
    }
    public function test7infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'QixQix',
            $objectFooBarQix->infQixFooService(7)
        );
    }
    public function test8infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'InfInfInf',
            $objectFooBarQix->infQixFooService(8)
        );
    }
    public function test9infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Foo',
            $objectFooBarQix->infQixFooService(9)
        );
    }
    public function test10infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '10',
            $objectFooBarQix->infQixFooService(10)
        );
    }
    public function test14infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Qix',
            $objectFooBarQix->infQixFooService(14)
        );
    }
    public function test15infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Foo',
            $objectFooBarQix->infQixFooService(15)
        );
    }
    public function test21infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Qix;Foo',
            $objectFooBarQix->infQixFooService(21)
        );
    }
    public function test24infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Inf;Foo',
            $objectFooBarQix->infQixFooService(24)
        );
    }
    public function test33infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooFooFoo',
            $objectFooBarQix->infQixFooService(33)
        );
    }
    public function test34infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Foo',
            $objectFooBarQix->infQixFooService(34)
        );
    }
    public function test35infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'QixFooInf',
            $objectFooBarQix->infQixFooService(35)
        );
    }

    public function test42infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Qix;Foo',
            $objectFooBarQix->infQixFooService(42)
        );
    }
    public function test43infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Foo',
            $objectFooBarQix->infQixFooService(43)
        );
    }
    public function test47infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Qix',
            $objectFooBarQix->infQixFooService(47)
        );
    }
    public function test48infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Inf;FooInf',
            $objectFooBarQix->infQixFooService(48)
        );
    }
    public function test52infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '52',
            $objectFooBarQix->infQixFooService(52)
        );
    }
    public function test55infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '55',
            $objectFooBarQix->infQixFooService(55)
        );
    }
    public function test57infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'FooQix',
            $objectFooBarQix->infQixFooService(57)
        );
    }
    public function test58infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Inf',
            $objectFooBarQix->infQixFooService(58)
        );
    }
    public function test70infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'QixQix',
            $objectFooBarQix->infQixFooService(70)
        );
    }
    public function test71infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'QixInf',
            $objectFooBarQix->infQixFooService(71)
        );
    }
    public function test73infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'QixFoo',
            $objectFooBarQix->infQixFooService(73)
        );
    }
    public function test83infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'InfFoo',
            $objectFooBarQix->infQixFooService(83)
        );
    }
    public function test88infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'InfInfInfInf',
            $objectFooBarQix->infQixFooService(88)
        );
    }
    public function test100infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            '100',
            $objectFooBarQix->infQixFooService(100)
        );
    }
    public function test120infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->assertEquals(
            'Inf;Foo',
            $objectFooBarQix->infQixFooService(120)
        );
    }

    public function testNegative1infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->expectException(InvalidArgumentException::class);
        $this->assertEquals(
            '-1',
            $objectFooBarQix->infQixFooService(-1)
        );
    }
    public function testNegative3infQixFooService()
    {
        $objectFooBarQix = new FooBarQix();
        $this->expectException(InvalidArgumentException::class);
        $this->assertEquals(
            '-3',
            $objectFooBarQix->infQixFooService(-3)
        );
    }
    
}
